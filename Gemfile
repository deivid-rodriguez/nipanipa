# frozen_string_literal: true

source "https://rubygems.org"

gem "bootsnap", "~> 1.4"
gem "rails", "~> 5.2"
gem "rails-i18n", "~> 5.1"
gem "rinku", "~> 2.0"

gem "dotenv-rails", "~> 2.7"

gem "bcrypt", "~> 3.1"
gem "carrierwave", "~> 2.0"
gem "cocoon", "~> 1.2"
gem "devise", "~> 4.7"
gem "enumerize", "~> 2.3"
gem "kaminari", "~> 1.1"
gem "mini_magick", "~> 4.9"
gem "pg", "1.1.4"
gem "simple_form", "~> 5.0"

# Frontend stuff
gem "autoprefixer-rails", "~> 9.7"
gem "bootstrap-sass", "~> 3.4"
gem "coffee-rails", "~> 5.0"
gem "jquery-rails", "~> 4.3"
gem "sass-rails", "~> 6.0"
gem "uglifier", "~> 4.2"

gem "activeadmin", "~> 2.4"

gem "ruby-progressbar", "~> 1.10", require: false
gem "rubyzip", "~> 1.2", require: false

gem "slim-rails", "~> 3.2"

group :tools do
  gem "brakeman", "~> 4.7"
  gem "byebug", "~> 11.0"
  gem "factory_bot_rails", "~> 5.1"
  gem "i18n-tasks", "0.9.29"
  gem "image_optim", "0.26.5"
  gem "rubocop", "0.76.0"
  gem "rubocop-performance", "~> 1.5"
  gem "rubocop-rspec", "~> 1.37"
  gem "simplecov", "0.17.1"
  gem "slim_lint", "0.18.0"
  gem "squasher", "0.6.2"
end

gem "rspec-rails", "~> 3.9", groups: %i[development test]

group :deploy do
  gem "capistrano-passenger", "0.2.0"
  gem "capistrano-pending", "0.2.0"
  gem "capistrano-rails", "~> 1.4"
  gem "capistrano-rbenv", "~> 2.1"
end

group :development do
  gem "dependabot-omnibus", "0.115.0"
  gem "letter_opener", "~> 1.7"
  gem "localeapp", "~> 3.1"
  gem "spring", "~> 2.1"
  gem "unicorn-rails", "~> 2.2"
end

group :test do
  gem "apparition", "0.3.0"
  gem "capybara", "~> 3.25"
  gem "database_cleaner", "~> 1.7"
  gem "launchy", "~> 2.4"
  gem "shoulda-matchers", "~> 4.1"
  gem "webmock", "~> 3.7"
end
