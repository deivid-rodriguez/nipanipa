# frozen_string_literal: true

return unless %w[development test].include?(Rails.env)

require "open3"

#
# Common stuff for a linter
#
module LinterMixin
  def run
    offenses = []

    applicable_files.each do |file|
      if clean?(file)
        print "."
      else
        offenses << file
        print "F"
      end
    end

    print "\n"

    return if offenses.empty?

    raise failure_message_for(offenses)
  end

  private

  def relative_path_to_self
    Pathname.new(__FILE__).relative_path_from(root).to_s
  end

  def root
    Pathname.new(File.expand_path("../..", __dir__))
  end

  def applicable_files
    Open3.capture2("git grep -Il ''")[0].split
  end

  def failure_message_for(offenses)
    msg = "#{self.class.name} detected offenses. "

    msg += if respond_to?(:fixing_cmd)
             "Run `#{fixing_cmd(offenses)}` to fix them."
           else
             "Affected files: #{offenses.join(' ')}"
           end

    msg
  end
end

#
# Lints executability of files
#
class ExecutableLinter
  include LinterMixin

  def applicable_files
    Open3.capture2("git ls-files")[0].split
  end

  def clean?(file)
    in_exec_folder = !(%r{bin/} =~ file).nil?
    executable = File.executable?(file) && !File.directory?(file)

    (in_exec_folder && executable) || (!in_exec_folder && !executable)
  end
end

#
# Checks trailing whitespace
#
class TrailingWhitespaceLinter
  include LinterMixin

  def clean?(file)
    File.read(file, encoding: Encoding::UTF_8) !~ / +$/
  end
end

#
# Checks no tabs in source code
#
class TabLinter
  include LinterMixin

  def clean?(file)
    file == relative_path_to_self ||
      File.directory?(file) ||
      !File.read(file, encoding: Encoding::UTF_8).include?("	")
  end
end

#
# Checks for optimizable images
#
class OptimizableImagesLinter
  include LinterMixin

  def applicable_files
    Dir.glob("app/assets/images/**")
  end

  def clean?(file)
    ImageOptim.new(
      advpng: false,
      pngout: false,
      svgo: false,
      jhead: false,
      jpegtran: false
    ).optimize_image(file).nil?
  end
end

desc "Lints code base"
task lint: :"lint:all"

namespace :lint do
  require "rubocop/rake_task"
  desc "Checks ruby code style with RuboCop"
  RuboCop::RakeTask.new

  require "slim_lint/rake_task"
  desc "Checks slim template code style with slim_lint"
  SlimLint::RakeTask.new { |t| t.files = ["app/views"] }

  desc "Checks for unnecessary execute permissions"
  task :executables do
    puts "Checking for unnecessary executables"

    ExecutableLinter.new.run
  end

  desc "Checks for unnecessary trailing whitespace across all repo files"
  task :trailing_whitespace do
    puts "Checking for unnecessary trailing whitespace..."

    TrailingWhitespaceLinter.new.run
  end

  desc "Checks for tabs"
  task :tabs do
    puts "Checking for unnecessary tabs"

    TabLinter.new.run
  end

  desc "Checks for optimizable images"
  task :optimizable_images do
    require "image_optim"
    puts "Checking for optimizable images"

    OptimizableImagesLinter.new.run
  end

  desc "Runs all linters"
  task all: %i[rubocop slim_lint executables trailing_whitespace tabs optimizable_images]
end
