FROM registry.gitlab.com/deivid-rodriguez/docker/rails:alpine

MAINTAINER David Rodriguez <deivid.rodriguez@riseup.net>

RUN mkdir -p /usr/local/etc \
  && { \
    echo 'install: --no-document'; \
    echo 'update: --no-document'; \
  } >> /usr/local/etc/gemrc

RUN DONT_USE_BUNDLER_FOR_GEMDEPS=true gem update --system 3.0.3
RUN gem install bundler --version 2.0.1

ENV BROWSER_PATH /usr/bin/chromium-browser

RUN apk add --no-cache \
  git \
  postgresql-dev \
  imagemagick \
  tzdata \
  chromium \
  gifsicle \
  jpegoptim \
  optipng \
  pngcrush \
  pngquant

ENV BUNDLE_CACHE_ALL_PLATFORMS true
ENV BUNDLE_GLOBAL_PATH_APPENDS_RUBY_SCOPE true
ENV BUNDLE_PATH .bundle
ENV BUNDLE_FROZEN true
ENV BUNDLE_WITHOUT deploy

RUN addgroup -g 1000 -S nipanipa && adduser -u 1000 -S nipanipa -G nipanipa
RUN mkdir /app && chown -R nipanipa:nipanipa /app

WORKDIR /app

RUN apk add --no-cache --virtual build-dependencies \
  gcc \
  g++ \
  libc-dev \
  linux-headers \
  make

USER nipanipa

COPY --chown=nipanipa:nipanipa Gemfile Gemfile.lock ./
RUN bundle install

USER root

RUN apk del build-dependencies

USER nipanipa

COPY --chown=nipanipa:nipanipa package.json package-lock.json ./
RUN npm ci

COPY --chown=nipanipa:nipanipa . ./

RUN bin/rails assets:precompile

CMD bin/rails s
